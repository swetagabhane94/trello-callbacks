const fs = require("fs");

function callback1(boards, boardId, callback) {
  setTimeout(() => {
    fs.readFile(boards, "utf-8", (err, data) => {
      if (err) {
        console.error(`Error occured in read file: ${err}`);
      } else {
        console.log("Reading boards file done");
        let checkBoardId = JSON.parse(data).filter((element) => {
          return element.id === boardId;
        });

        callback(null, checkBoardId);
      }
    });
  }, 2 * 1000);
}

module.exports = callback1;
