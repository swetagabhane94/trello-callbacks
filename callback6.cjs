const fs = require("fs");

const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

function callback6(lists, boards, cards, thanos) {
  setTimeout(() => {
    callback1(boards, thanos, (err, boardDetail) => {
      if (err) {
        console.error(err);
      } else {
        console.log(boardDetail);

        callback2(lists, thanos, (err, listDetail) => {
          if (err) {
            console.error(err);
          } else {
            console.log(listDetail);

            const matchList = listDetail.map((element) => {
              return element.id;
            });

            for (let index = 0; index < matchList.length; index++) {
              callback3(cards, matchList[index], (err, cardsDetail) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log(cardsDetail);
                }
              });
            }
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = callback6;
