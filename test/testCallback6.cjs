const callback6 = require("../callback6.cjs");
const path = require("path");

const boards = path.resolve("../boards.json");
const lists = path.resolve("../lists.json");
const cards = path.resolve("../cards.json");
const thanos = "mcu453ed";

callback6(lists, boards, cards, thanos, (err, cardsDetail) => {
  if (err) {
    console.error(err);
  } else {
    console.log(cardsDetail);
  }
});
