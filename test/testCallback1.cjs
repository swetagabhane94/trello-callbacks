const callback1 = require("../callback1.cjs");
const path = require("path");

const boards = path.resolve("../boards.json");
const boardId = "mcu453ed";

callback1(boards, boardId, (err, boardDetail) => {
  if (err) {
    console.error(err);
  } else {
    console.log(boardDetail);
  }
});
