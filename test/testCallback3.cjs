const callback3 = require("../callback3.cjs");
const path = require("path");

const cards = path.resolve("../cards.json");
const listId = "qwsa221";

callback3(cards, listId, (err, cardsDetail) => {
  if (err) {
    console.error(err);
  } else {
    console.log(cardsDetail);
  }
});
