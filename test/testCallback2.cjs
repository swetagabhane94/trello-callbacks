const callback2 = require("../callback2.cjs");
const path = require("path");

const lists = path.resolve("../lists.json");
const boardId = "abc122dc";

callback2(lists, boardId, (err, listDetail) => {
  if (err) {
    console.error(err);
  } else {
    console.log(listDetail);
  }
});
