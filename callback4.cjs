const fs = require("fs");

const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

function callback4(lists, boards, cards, thanos) {
  setTimeout(() => {
    callback1(boards, thanos, (err, boardDetail) => {
      if (err) {
        console.error(err);
      } else {
        console.log(boardDetail);

        callback2(lists, thanos, (err, listDetail) => {
          if (err) {
            console.error(err);
          } else {
            console.log(listDetail);

            const matchMind = listDetail.filter((element) => {
              return element.name == "Mind";
            });

            callback3(cards, matchMind[0].id, (err, cardsDetail) => {
              if (err) {
                console.error(err);
              } else {
                console.log(cardsDetail);
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = callback4;
