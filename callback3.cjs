const fs = require("fs");

function callback3(cards, listId, callback) {
  setTimeout(() => {
    fs.readFile(cards, "utf-8", (err, data) => {
      if (err) {
        console.error(`Error occured in read file: ${err}`);
      } else {
        console.log("Reading cards file done");
        let value = JSON.parse(data)[listId];

        if (value !== undefined) {
          callback(null, value);
        }
      }
    });
  }, 2 * 1000);
}

module.exports = callback3;
