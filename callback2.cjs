const fs = require("fs");

function callback2(lists, boardId, callback) {
  setTimeout(() => {
    fs.readFile(lists, "utf-8", (err, data) => {
      if (err) {
        console.error(`Error occured in read file: ${err}`);
      } else {
        console.log("Reading list file done");
        let value = JSON.parse(data)[boardId];

        callback(null, value);
      }
    });
  }, 2 * 1000);
}

module.exports = callback2;
